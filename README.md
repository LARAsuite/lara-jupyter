# lara_jupyter  - A LARA jupyter-lab notebook collection.

## Installation of jupyter-lab

    python3.8 -m venv $HOME/py3venv/lara_venv
    source $HOME/py3venv/lara_venv/bin/activate
    # in activated venv call:
    pip install  wheel ipykernel  jupyterlab
    python -m ipykernel install --user --name=lara_venv

    # to call jupyter-lab just tpye
    jupyter-lab 
    

## Generating documentation

To generate the documentation, please change directory to app and run:

    sphinx-apidoc -o docs . 
    make html

## Acknowledgements

The LARA-django developers thank

    -  the python team
    -  the whole [django team](<https://www.djangoproject.com/) for their great framework !

